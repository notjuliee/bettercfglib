#include <cstdio>

#define CFG_GEN_SRC
#include "test.h"

int main() {
    bcfg_test *test = bcfg_parse_test("test.cfg");
    printf("[foo.biz] = %s\n", test->foo.biz);
    printf("[foo.bar] = %d\n", test->foo.bar);
    test->foo.bar += 1;
    bcfg_write_test(test, "test.cfg");
    bcfg_free_test(test);
    return 0;
}
