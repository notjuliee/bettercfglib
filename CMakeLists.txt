cmake_minimum_required(VERSION 3.17)
project(BetterCfgLib C CXX)

set(CMAKE_C_STANDARD 11)

add_executable(BetterCfgLib main.cpp)
target_include_directories(BetterCfgLib PUBLIC include)