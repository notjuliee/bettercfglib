#ifndef GEN_CFG
#define CFG_NAME test
#define CFG_PATH test.h
#include "cfg_base.h"
#else

SECTION(foo, "test section")
OPTION(foo, const char *, biz, "hello, world", "test string")
OPTION(foo, int, bar, 10, "test int")
END_SECTION(foo)

#endif