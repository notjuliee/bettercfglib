#ifndef CFG_NAME
#error cfg_base included without setting CFG_NAME
#endif
#ifndef CFG_PATH
#error cfg_base included without setting CFG_PATH
#endif

#include <stdbool.h>

#define __X(x) #x
#define _X(x) __X(x)
#define __CAT(x, y) x##y
#define _CAT(x, y) __CAT(x, y)

#define GEN_CFG

// First, generate our structs
#define SECTION(name, description) typedef struct {
#define OPTION(section, type, name, default_value, description) type name;
#define END_SECTION(name)                                                                                              \
    }                                                                                                                  \
    _CAT(_CAT(_CAT(bcfg_, CFG_NAME), _s_), name);
#include _X(CFG_PATH)
#undef SECTION
#undef OPTION
#undef END_SECTION

#define SECTION(name, description)                                                                                     \
    _CAT(_CAT(_CAT(bcfg_, CFG_NAME), _s_), name)                                                                       \
    name;
#define OPTION(section, type, name, default_value, description)
#define END_SECTION(name)
typedef struct {
#include _X(CFG_PATH)
    void *_internal;
} _CAT(bcfg_, CFG_NAME);
#undef SECTION
#undef OPTION
#undef END_SECTION

#ifdef __cplusplus
#define PREFIX extern "C"
#else
#define PREFIX
#endif

PREFIX _CAT(bcfg_, CFG_NAME) * _CAT(bcfg_parse_, CFG_NAME)(const char *filename);
PREFIX void _CAT(bcfg_write_, CFG_NAME)(const _CAT(bcfg_, CFG_NAME) * cfg, const char *filename);
PREFIX void _CAT(bcfg_free_, CFG_NAME)(_CAT(bcfg_, CFG_NAME) * cfg);

#undef PREFIX

#ifdef CFG_GEN_SRC
#ifndef __cplusplus
#error parser source must be generated in a C++ file
#endif

#include <cstdlib>
#include <cstring>

namespace bcfg {
bool is_whitespace(const char *c) {
    switch (*c) {
    case ' ':
    case '\t':
    case '\n':
        return true;
    default:
        return false;
    }
}

bool is_valid_identifier(const char *c) {
    if (*c == '_')
        return true;
    if (*c >= '0' && *c <= '9')
        return true;
    if (*c >= 'A' && *c <= 'Z')
        return true;
    if (*c >= 'a' && *c <= 'z')
        return true;
    return false;
}

const char *substr(const char *src, size_t len) {
    char *buffer = static_cast<char *>(malloc(len + 1));
    buffer[len] = '\0';
    strncpy(buffer, src, len);
    return buffer;
}

template <typename T> struct cfg_val {
    static T parse(char *start, size_t len);
    static const char *unparse(T val);
};
template <> struct cfg_val<const char *> {
    static const char *parse(char *start, size_t len) {
        if (*start != '"') {
            printf("Invalid string ><\n");
            // TODO: Report errors back up
        }
        if (*(start + len - 1) != '"') {
            printf("Invalid string ><\n");
        }
        *(start + len - 1) = '\0';
        return start + 1;
    }
    static const char *unparse(const char *val) {
        size_t len = strlen(val);
        char *buffer = static_cast<char *>(malloc(len + 3));
        buffer[0] = '"';
        strcpy(buffer + 1, val);
        buffer[len + 2] = '\0';
        buffer[len + 1] = '"';
        printf("%s -> %s\n", val, buffer);

        return buffer;
    }
};
template <> struct cfg_val<int> {
    static int parse(char *start, size_t len) {
        const char *str = substr(start, len);
        int val = -1;
        if (sscanf(str, "%d", &val) != 1) {
            printf("Invalid number ><\n");
        }
        return val;
    }
    static const char *unparse(int val) {
        int length = snprintf(nullptr, 0, "%d", val);
        char *buffer = static_cast<char *>(malloc(length + 1));
        snprintf(buffer, length + 1, "%d", val);

        return buffer;
    }
};
} // namespace bcfg

constexpr size_t _CAT(bcfg_i_num_opts_, CFG_NAME)() {
    size_t i = 0;
#define SECTION(...)
#define OPTION(...) i++;
#define END_SECTION(...)
#include _X(CFG_PATH)
#undef SECTION
#undef OPTION
#undef END_SECTION
    return i;
}

#define SECTION(...)
#define OPTION(section, type, name, default_value, description) _CAT(_CAT(section, __), name),
#define END_SECTION(...)
enum class _CAT(bcfg_iopts_, CFG_NAME) {
    _Undefined,
#include _X(CFG_PATH)
};
#undef SECTION
#undef OPTION
#undef END_SECTION

typedef struct {
    char *pos;
    size_t len;
    _CAT(bcfg_iopts_, CFG_NAME)
    type;
} _CAT(bcfg_i_pos_, CFG_NAME);

#define SECTION(name, description) "[" #name "]  // " description "\n"
#define OPTION(section, type, name, default_value, description) #name " = " #default_value "  // " description "\n"
#define END_SECTION(name) "\n\n"
const char *_CAT(_CAT(BCFG_DEFAULT_, CFG_NAME), _STR) =
#include _X(CFG_PATH)
    ;
#undef SECTION
#undef OPTION
#undef END_SECTION

typedef struct {
    char *_content;
    size_t _content_len;
    _CAT(bcfg_i_pos_, CFG_NAME)
    _markers[_CAT(bcfg_i_num_opts_, CFG_NAME)()];
} _CAT(bcfg_i_, CFG_NAME);

#define _Z(x) ((_CAT(bcfg_i_, CFG_NAME) *)x->_internal)

#define SECTION(name, description) name,
#define OPTION(...)
#define END_SECTION(...)
enum class _CAT(bcfg_is_, CFG_NAME) {
    _Undefined,
#include _X(CFG_PATH)
};
#undef SECTION
#undef OPTION
#undef END_SECTION

extern "C" _CAT(bcfg_, CFG_NAME) * _CAT(bcfg_parse_, CFG_NAME)(const char *filename) {
    auto temp = new _CAT(bcfg_, CFG_NAME);
    temp->_internal = (void *)new _CAT(bcfg_i_, CFG_NAME);

    // Load our _content
    if (filename != nullptr) {
        FILE *f = fopen(filename, "r");
        fseek(f, 0L, SEEK_END);
        _Z(temp)->_content_len = ftell(f);
        rewind(f);
        _Z(temp)->_content = new char[_Z(temp)->_content_len];
        fread((void *)_Z(temp)->_content, 1, _Z(temp)->_content_len, f);
        fclose(f);
    } else {
        _Z(temp)->_content_len = strlen(_CAT(_CAT(BCFG_DEFAULT_, CFG_NAME), _STR));
        _Z(temp)->_content = new char[_Z(temp)->_content_len];
        memcpy((void *)_Z(temp)->_content, _CAT(_CAT(BCFG_DEFAULT_, CFG_NAME), _STR), _Z(temp)->_content_len);
    }

    _CAT(bcfg_is_, CFG_NAME)
    cur_section = _CAT(bcfg_is_, CFG_NAME)::_Undefined;
    const char *token_start = nullptr;
    const char *token_end = nullptr;
    char *value_start = nullptr;
    const char *value_end = nullptr;
    bool equals_found = false;
    size_t cur_tag = 0;
    char *cur = _Z(temp)->_content;
    while (*cur != '\0') {
        // Ignore whitespace
        while (bcfg::is_whitespace(cur)) {
            cur++;
        }
        if (*cur == '\0')
            break;

        // Ignore comments
        if (*cur == '/' && *(cur + 1) == '/') {
            cur += 2;
            while (*cur != '\n' && *cur != '\0') {
                cur++;
            }
            continue;
        }

        // Check for sections
        if (*cur == '[') {
            cur++;
            char *section_start = cur;
            while (true) {
                if (*cur == ']')
                    break;
                if (!bcfg::is_valid_identifier(cur)) {
                    printf("Invalid character: %c\n", *cur);
                    // Invalid identifier
                    return nullptr;
                }
                cur++;
            }
            size_t section_len = cur - section_start;
            cur++;
#ifndef NDEBUG
            const char *sec_str = bcfg::substr(section_start, section_len);
            printf("Section: %s\n", sec_str);
            free((void *)sec_str);
#endif

#define SECTION(name, description)                                                                                     \
    else if (strncmp(#name, section_start, section_len) == 0) {                                                        \
        cur_section = _CAT(bcfg_is_, CFG_NAME)::name;                                                                  \
    }
#define OPTION(...)
#define END_SECTION(...)
            if (false) {
            }
#include _X(CFG_PATH)
#undef SECTION
#undef OPTION
#undef END_SECTION
            else {
                // Invalid section name
                return nullptr;
            }
            continue;
        }

        if (cur_section != bcfg_is_test::_Undefined) {
            if (token_start == nullptr) {
                while (bcfg::is_valid_identifier(cur)) {
                    if (token_start == nullptr)
                        token_start = cur;
                    cur++;
                }
                if (token_start != nullptr) {
                    token_end = cur;
                }
                while (bcfg::is_whitespace(cur)) {
                    cur++;
                }
                if (*cur != '=') {
                    printf("Expected =, got %c", *cur);
                    return nullptr;
                }
                if (token_start != nullptr) {
#ifndef NDEBUG
                    const char *token_str = bcfg::substr(token_start, token_end - token_start);
                    printf("Token: %s (%ld)\n", token_str, token_end - token_start);
                    free((void *)token_str);
#endif
                }
                cur++;
                continue;
            } else {
                value_start = cur;
                while (*cur != '\n' && *cur != '\0') {
                    if (*cur == '/' && *(cur + 1) == '/')
                        break;
                    cur++;
                }
                while (bcfg::is_whitespace(cur - 1)) {
                    cur--;
                }
                value_end = cur;
#ifndef NDEBUG
                const char *value_str = bcfg::substr(value_start, value_end - value_start);
                printf("Value: %s (%ld)\n", value_str, value_end - value_start);
                free((void *)value_str);
#endif

#define SECTION(name, description) else if (cur_section == _CAT(bcfg_is_, CFG_NAME)::name) {
#define OPTION(section, vtype, name, default_value, description)                                                       \
    if (strncmp(#name, token_start, token_end - token_start) == 0) {                                                   \
        _Z(temp)->_markers[cur_tag].pos = value_start;                                                                 \
        _Z(temp)->_markers[cur_tag].len = value_end - value_start;                                                     \
        _Z(temp)->_markers[cur_tag].type = _CAT(bcfg_iopts_, CFG_NAME)::_CAT(_CAT(section, __), name);                 \
        cur_tag++;                                                                                                     \
        temp->section.name = bcfg::cfg_val<vtype>::parse(value_start, value_end - value_start);                        \
    }
#define END_SECTION(name) }
                if (false) {
                }
#include _X(CFG_PATH)
#undef SECTION
#undef OPTION
#undef END_SECTION

                token_start = token_end = value_end = value_start = nullptr;
                continue;
            }
        }

        printf("Unexpected character: %c\n", *cur);
        cur++;
    }

    return temp;
}

extern "C" void _CAT(bcfg_free_, CFG_NAME)(_CAT(bcfg_, CFG_NAME) * cfg) {
    delete[] _Z(cfg)->_content;
    delete (_CAT(bcfg_i_, CFG_NAME) *)cfg->_internal;
    delete cfg;
}

extern "C" void _CAT(bcfg_write_, CFG_NAME)(const _CAT(bcfg_, CFG_NAME) * cfg, const char *filename) {
    FILE *f = fopen(filename, "w+");

    char *cur = _Z(cfg)->_content;

    // Go through all our markers to start adding to the file
    for (size_t i = 0; i < _CAT(bcfg_i_num_opts_, CFG_NAME)(); i++) {
        // Write the predule leading up to it
        fwrite((void *)cur, 1, _Z(cfg)->_markers[i].pos - cur, f);
        cur = _Z(cfg)->_markers[i].pos + _Z(cfg)->_markers[i].len;

#define SECTION(...)
#define OPTION(section, type, name, default_value, description)                                                        \
    case _CAT(bcfg_iopts_, CFG_NAME)::_CAT(_CAT(section, __), name): {                                                 \
        const char *temp = bcfg::cfg_val<type>::unparse(cfg->section.name);                                            \
        fprintf(f, "%s", temp);                                                                                        \
        free((void *)temp);                                                                                            \
        break;                                                                                                         \
    }
#define END_SECTION(...)
        switch (_Z(cfg)->_markers[i].type) {
#include _X(CFG_PATH)
        case bcfg_iopts_test::_Undefined:
            fprintf(f, "??????");
        }
#undef SECTION
#undef OPTION
#undef END_SECTION
    }
    // If we haven't gotten through the entire file, dump the rest of it
    if (cur < _Z(cfg)->_content + _Z(cfg)->_content_len) {
        fwrite((void *)cur, 1, _Z(cfg)->_content_len - (cur - _Z(cfg)->_content), f);
    }

    fclose(f);
}

#undef _Z

#endif

#undef __CAT
#undef _CAT
#undef __X
#undef _X
#undef GEN_CFG
#undef CFG_NAME
#undef CFG_PATH