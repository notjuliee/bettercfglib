All the existing C++ config libraries have a terrible API, this is my single header C++ config library that, in my opinion, doesn't absolutely suck.  

### Usage:
To install the library, simply copy `include/cfg_base.h` to somewhere in your include path.  
To declare a new config schema, you must create a new header file and a new c++ source file.  

The header file should be in the following format:  
```cpp
#ifndef GEN_CFG
#define CFG_NAME <name of your config schema>
#define CFG_PATH <path to include this header>
#include "<path to cfg_base.h>"
#else

SECTION(<section name>, "<section description>")
OPTION(<section name>, <option type>, <option name>, <default value>, "<option description>") // Repeat for all options in section
END_SECTION(<section name>) // Repeat for all sections in config

#endif
```

The c++ source file should be in the following format:  
```cpp
#define CFG_GEN_SRC
#include "<path to your config header>"
```

Once you've defined a schema like that, in any other C or C++ file you can use it with:  
```cpp
#include "<path to your config header>"

// Create a new config object from defaults
bcfg_<name of your config schema>* cfg = bcfg_parse_<name of your config schema>(NULL);
// Or create by loading a file
bcfg_<name of your config schema>* cfg = bcfg_parse_<name of your config schema>("<filename>");

// Access values
cfg-><section>.<option>;
// You can assign directly to values

// Write any changes you've made back to the file
bcfg_write_<name of your config schema>("<filename>");

// Free the config object once you're done with it
bcfg_free_<name of your config schema>();
```
